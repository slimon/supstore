from django.apps import AppConfig


class SupstoreAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'supstore_app'

from django import forms
from django.forms import widgets
from .models import Category

class CategoryForm(forms.Form):
    title = forms.CharField(
        max_length=100, 
        required=True, 
        label="Title",
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Название категории'})
        )
    description = forms.CharField(
        max_length=2000, 
        required=False, 
        label="Category Description", 
        widget=widgets.Textarea(attrs={
            'class': 'form-control',
            'rows': 3})
        )

class ProductForm(forms.Form):
    name = forms.CharField(
        max_length=200, 
        required=True, 
        label='Name',
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Название товара'})
        )
    description = forms.CharField(
        max_length=3000, 
        required=False, 
        label='Product Description',
        widget=widgets.Textarea(attrs={
            'class': 'form-control',
            'rows': 3})
        )
    cat = forms.ModelChoiceField(
        required=True,
        label='Category',
        widget=widgets.Select(attrs={
            'class': 'form-control',
            'placeholder': 'Категория'}),
        queryset=Category.objects.all()
        )
    price = forms.DecimalField(
        max_digits=10,
        required=True,
        label='Price',
        widget=widgets.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': 'Цена товара'})
        )
    image = forms.CharField(
        max_length=2000,
        required=True,
        label='Image',
        widget=widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'URL изображения'})
        )

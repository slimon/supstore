from django.urls import path
from supstore_app.views.category_views import (
    category_add_view,
    categories_view,
    category_delete_view,
    category_edit_view,
    category_detail_view
)

urlpatterns = [
    path('add/', category_add_view, name='category_add'),
    path('', categories_view, name='categories_list'),
    path('delete/<int:pk>', category_delete_view, name='category_delete'),
    path('<int:pk>/edit', category_edit_view, name='category_edit'),
    path('detail/<int:pk>', category_detail_view, name='category_detail')
]
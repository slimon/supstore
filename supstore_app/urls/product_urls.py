from django.urls import path
from supstore_app.views.products_view import (
    products_view,
    product_add_view,
    product_view,
    product_edit_view,
    product_delete_view
)

urlpatterns = [
    path('', products_view, name='products_list'),
    path('add/', product_add_view, name='product_add'),
    path('detail/<int:pk>', product_view, name='product_detail'),
    path('<int:pk>/edit', product_edit_view, name='product_edit'),
    path('delete/<int:pk>', product_delete_view, name='product_delete')
]
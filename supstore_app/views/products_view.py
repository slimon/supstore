from django.shortcuts import render, redirect, get_object_or_404
from ..models import Product, Category
from ..forms import ProductForm

def products_view(request):
    products = Product.objects.all()
    return render(request, 'products/list.html', context={
        'products': products
    })


def product_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk=kwargs.get('pk'))
    return render(request, 'products/detail.html', 
                  context={'product': product})


def product_add_view(request):
    if request.method == 'GET':
        form = ProductForm()
        return render(request, 'products/add.html', context={
            'form': form,
            'categories': Category.objects.all()
            })
    elif request.method == 'POST':
        form = ProductForm(data=request.POST)
        if form.is_valid():
            category = Category.objects.get(pk=request.POST.get('cat'))
            new_product = Product.objects.create(
                name=form.cleaned_data['name'],
                price=form.cleaned_data['price'],
                cat=category,
                image=form.cleaned_data['image'],
                description=form.cleaned_data['description']
            )
            return redirect('product_detail', pk=new_product.pk)
        else:
            return render(request, 'products/add.html', context={'form': form})
        
def product_edit_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk=kwargs.get('pk'))
    if request.method == 'GET':
        form = ProductForm(initial={
            'name': product.name,
            'price': product.price,
            'image': product.image,
            'description': product.description,
            'cat': product.cat
        })
        return render(request, 'products/edit.html', context={
            'form': form,
            'product': product
        })
    elif request.method == 'POST':
        form = ProductForm(data=request.POST)
        if form.is_valid():
            product.name = form.cleaned_data.get('name')
            product.price = form.cleaned_data.get('price')
            product.image = form.cleaned_data.get('image')
            product.description = form.cleaned_data.get('description')
            product.cat = form.cleaned_data.get('cat')
            product.save()
            return redirect('product_detail', pk=product.pk)
        else:
            return render(request, 'products/edit.html', context={
                'form': form,
                'product': product
            })
        
def product_delete_view(request, *args, **kwargs):
    product = get_object_or_404(Product, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'products/delete.html', context={'product': product})
    if request.method == 'POST':
        product.delete()
        return redirect('products_list')
    
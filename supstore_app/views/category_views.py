from django.shortcuts import render, redirect, get_object_or_404
from ..models import Category, Product
from ..forms import CategoryForm

def index_view(request):
    products = Product.objects.order_by('pk')[0:4]
    return render(request, 'index.html', context={
        'products': products
    })


def category_add_view(request):
    if request.method == 'GET':
        form = CategoryForm()
        return render(request, 'categories/add.html', context={
            'form': form
        })
    elif request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            new_category = Category.objects.create(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description']
            )
            return render(request, 'index.html')
        else:
            return render(request, 'categories/add.html', context={
                'form': form
            })


def category_detail_view(request, *args, **kwargs):
    category = get_object_or_404(Category, pk=kwargs.get('pk'))
    return render(request, 'categories/detail.html', context={
        'category': category
    })


def categories_view(request):
    categories = Category.objects.all()
    return render(request, 'categories/list.html', context={
        'categories': categories
    })


def category_delete_view(request, *args, **kwargs):
    category = get_object_or_404(Category, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'categories/delete.html', context={'category': category})
    elif request.method == 'POST':
        category.delete()
        return redirect('home_page')
    
    
def category_edit_view(request, *args, **kwargs):
    category = get_object_or_404(Category, pk=kwargs.get('pk'))
    if request.method == 'GET':
        form = CategoryForm(initial={
            'title': category.title,
            'description': category.description
        })
        return render(request, 'categories/edit.html', context={
            'form': form,
            'category': category
        })
    elif request.method == 'POST':
        form = CategoryForm(data=request.POST)
        if form.is_valid():
            category.title = form.cleaned_data.get('title')
            category.description = form.cleaned_data.get('description')
            category.save()
            return redirect('categories_list')
        else:
            return render(request, 'categories/edit.html', context={
                'form': form,
                'category': category
            })
from django.db import models

class Category(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Название категории')
    description = models.TextField(max_length=2000, null=True, blank=True, verbose_name='Описание категории')

    def __str__(self):
        return self.title

class Product(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name='Наименование товара')
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Описание товара')
    cat = models.ForeignKey(to=Category, on_delete=models.CASCADE, null=True, verbose_name='Категория')
    added_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата добавления')
    updated_date = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    image = models.CharField(max_length=2000, null=False, blank=False, verbose_name='Изображение товара')
    
    def __str__(self):
        return f'#{self.pk}: {self.name}'
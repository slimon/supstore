from django.contrib import admin
from .models import Category, Product

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['title']
    search_fields = ['title']
    fields = ['title', 'description']

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'added_date', 'cat']
    list_filter = ['name', 'cat']
    search_fields = ['name', 'price', 'cat']
    fields = ['name', 'description', 'price', 'cat']
    readonly_fields = ['added_date', 'updated_date']

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)